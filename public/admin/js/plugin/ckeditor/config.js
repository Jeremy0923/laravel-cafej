/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.language = 'zh-cn';
	//config.filebrowserImageUploadUrl = 'http://localhost:8081/crowDesign/app/filemanager/dialog.php';
 	config.filebrowserBrowseUrl = '/wefree/public/ckfinder/ckfinder.html';   
    config.filebrowserImageBrowseUrl = '/wefree/public/ckfinder/ckfinder.html?Type=Images';  
    config.filebrowserFlashBrowseUrl = '/wefree/public/ckfinder/ckfinder.html?Type=Flash';  
};
