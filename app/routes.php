<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::route('login');
});

Route::get('/login',function()
{
	return View::make('home.login');
});
Route::post('/login',array('as'=>'login',function()
{
	$user = array(
		'username'=>Input::get('username'),
		'password'=>Input::get('password')
		);
	if(Auth::attempt($user))
   		return Redirect::to('/admin/index');
   	return Redirect::route('login')
		->with('flash_error','用户名或密码错误！')
		->withInput();
}));
Route::get('/logout',function()
{
	Auth::logout();
	return Redirect::to('/login');
});


Route::group(array('prefix'=>'admin'),function()
{
	Route::get('/index',function(){
		return Redirect::to('admin/posts');
	});
	Route::resource('/posts', 'PostsController');
});

