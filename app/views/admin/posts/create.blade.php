@extends("admin.layout.dashboard")
@section("content")
<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
		<h2>添加Posts</h2>				
	</header>
	<div>
		<div class="jarviswidget-editbox"></div>
		<div class="widget-body no-padding">
			{{ Form::open(['url' => 'admin/posts','id'=>'create-form','class'=>'smart-form']) }}
				<header>
					添加Posts
				</header>
				<fieldset>
				<div class="row">
					<section class="col col-6">
					{{Form::label('title', 'Posts title', ['class'=>'label'])}}
						<label class="input"> <i class="icon-append fa  fa-file"></i>
							{{Form::text('title','',['id'=>'title'])}}
						</label>
					</section>
				</div>
				<div class="row">
					<section class="col col-6">
					{{Form::label('body', 'Posts body', ['class'=>'label'])}}
					{{Form::textarea('body','',['id'=>'body'])}}
					</section>
				</div>
				</fieldset>
				<footer>
					{{Form::submit('新增',['class'=>'btn btn-primary'])}}
				</footer>
			{{ Form::close() }}
		</div>
		<!-- end widget content -->
	</div>
	<!-- end widget div -->
</div>
<!-- end widget -->		
@stop
@section("jslib")
{{ HTML::script('admin/js/plugin/jquery-validate/jquery.validate.min.js'); }}
{{ HTML::script('admin/js/plugin/jquery-form/jquery-form.min.js'); }}
{{ HTML::script('admin/js/plugin/ckeditor/ckeditor.js'); }}
<script type="text/javascript">
$(document).ready(function() {
	CKEDITOR.replace( 'body', { height: '350px', startupFocus : true} );
	var $createForm = $("#create-form").validate({
				// Rules for form validation
				rules : {
					title : {
						required : true,
					},
					body : {
						required : true,
					}
				},
				// Messages for form validation
				messages : {
					title : {
						required : 'Posts title 不能为空',
					},
					body: {
						required : 'Posts body 不能为空',
					}
				},

				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
})
</script>
@stop