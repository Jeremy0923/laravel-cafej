@extends("admin.layout.dashboard")
@section("content")
<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i> </span>
		<h2>Posts</h2>
	</header>
	<div>
		<div class="jarviswidget-editbox"></div>	
		<div class="widget-body no-padding">
			<div class="widget-body-toolbar"></div><!-- 放置工具栏的地方 -->
			<table id="dt_basic" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>编号</th>
						<th>title</th>
						<th>body</th>
						<th>创建日期</th>
						<th>修改日期</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					@foreach($posts as $post)
					<tr>
						<td>{{$post->id}}</td>
						<td>{{$post->title}}</td>
						<td>{{$post->body}}</td>
						<td>{{$post->created_at}}</td>
						<td>{{$post->updated_at}}</td>
						<td>
							<a class="btn bg-color-blue txt-color-white" href="{{URL::to('admin/posts/'.$post->id)}}" >查看</a>
							<a class="btn bg-color-blue txt-color-white" href="{{ URL::to('admin/posts/' . $post->id . '/edit') }}" >修改</a>
							{{ Form::open(['class' => 'pull-right' ,'id'=>'delete-form']) }}
							{{ Form::hidden('_method', 'DELETE') }}
							<a class="btn bg-color-teal txt-color-white" href="javascript:remove('{{URL::to('admin/posts/'.$post->id)}}')" >删除</a> 
							{{ Form::close() }}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop
@section("jslib")
	{{ HTML::script('admin/js/plugin/datatables/jquery.dataTables-cust.min.js'); }}
	{{ HTML::script('admin/js/plugin/datatables/DT_bootstrap.js'); }}
	
	<script type="text/javascript">
	$(document).ready(function() {
		$('#dt_basic').dataTable({
			"sPaginationType" : "bootstrap_full"
		});
	})
	function remove(url)
	{
		$.SmartMessageBox({
			title:'提示信息',
			content:'数据删除后将无法恢复！您确认要删除么？',
			buttons : '[否][是]'

		}, function(ButtonPressed) {
			if (ButtonPressed == "是") {
				$("#delete-form").attr("action",url);
				$("#delete-form").submit();
			}

		});
	}
	</script>

@stop