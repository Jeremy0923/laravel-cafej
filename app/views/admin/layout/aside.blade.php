<nav>
	<ul>
		<li>
			<a href="#"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">首页</span></a>
		</li>
		<li>
			<a href="#"><i class="fa fa-lg fa-fw fa-leaf"></i> <span class="menu-item-parent">一级菜单</span></a>
			<ul>
				<li>
					<a href="#">二级菜单</a>
					<ul>
						<li><a href="#">三级菜单</a></li>
						<li><a href="#">三级菜单</a></li>
						<li><a href="#">三级菜单</a></li>
					</ul>
				</li>
				<li><a href="#">二级菜单</a></li>
				<li><a href="{{URL::to('/admin/posts')}}">Posts</a></li>
				<li><a href="{{URL::to('/admin/posts/create')}}">Posts create</a></li>
			</ul>
		</li>
	</ul>
</nav>
<span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>