<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>登陆</title>
    <!-- Bootstrap core CSS -->
    {{ HTML::style('common/css/bootstrap.min.css'); }}
    {{ HTML::style('home/css/signin.css'); }}
    <!-- Custom styles for this template -->
</head>
<body>
    <div class="col-lg-3">  
    </div>
    <div class="container">
    @if(Auth::guest())
    {{ Form::open(array('url' => 'login', 'method' => '{POST}', 'class'=>'form-signin' , 'role'=>'form')) }}
        {{Form::text('username','',['class'=>'form-control','placehodler'=>'用户名','required'=>true,'autofocus'=>true,Input::old("username")])}}
        {{Form::password('password',['class'=>'form-control','plcaeholder'=>'密码','required'=>true])}}
        {{Form::submit('登陆',['class'=>'btn btn-lg btn-primary btn-block'])}}
    {{ Form::close() }}
    @endif 
    @if(Auth::check())
        <div class="alert alert-warning fade in text-center">
            <p class="text-danger">您已经登陆，2s后页面将跳转至后台</p>
        </div>
        <script>
        window.setTimeout("window.location='{{URL::to('/admin/index')}}'",2000); 
        </script>
    @endif 
    </div>
    @if (Session::has('flash_error'))
    <div class="alert alert-warning fade in text-center">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <p class="text-danger">{{Session::get('flash_error')}}</p>
    </div>
    @endif
    {{ HTML::script('js/jquery_1.10.2.js'); }}
    {{ HTML::script('js/bootstrap.min.js'); }}
    <script type="text/javascript">
    $(document).ready(function() {
        $(".alert").alert('close')
    }
    </script>
</body>
</html>